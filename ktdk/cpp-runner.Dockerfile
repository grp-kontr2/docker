FROM alpine:latest

# Create kontr user
RUN adduser -D -g '' kontr

LABEL maintainer="Peter Stanko (peter.stanko0@gmail.com)"
LABEL image="kontr2/testing-ktdk-runner"

RUN apk update && \
    apk add git ca-certificates clang valgrind \
    python3 python3-dev cmake make gcc alpine-sdk

RUN pip3 install --upgrade pip && pip3 install pipenv






