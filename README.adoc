= Docker support scripts

Repository contains support docker scripts to build and deploy images.

== Maintainer
- Peter Stanko

== Structure

- `build` - contains scripts related to rebuild all the images based on current version in the repository
- `base` - base image with installed `pipenv` and `python 3.6` used for the `portal` image
- `portal` - contains docker compose files to pull and deploy images on multiple envs

== How to use

=== Build all images

[code, bash]
----
build/run.sh
----

=== Deploy env

[code, bash]
----
docker-compose -f portal/docker-compose.yml up
----

== Contribute
TBD


